# syntax = edrevo/dockerfile-plus
FROM ubuntu:focal as base
ARG DEBIAN_FRONTEND=noninteractive
# Dependencies
RUN apt-get update -y && apt-get install -y \
	git \
	make \
	gcc \
	wget \
	tar \
	patch \
	xz-utils \
	bc \
	xxd \
	build-essential \
	bison \
	flex \
	python3 \
	python3-distutils \
	python3-dev \
	swig \
	python \
	python-dev \
	kmod \
	curl \
	device-tree-compiler \
	apt-transport-https \
	software-properties-common \
	uuid-dev \
	iasl \
	nasm \
	libbrotli-dev \
	brotli \
	libssl-dev \
	linux-firmware \
	rsync \
	zstd \
	p7zip-full \
	unrar \
	unzip \
	u-boot-tools

# Get linaro gcc 7.5.0 gcc
RUN wget -q -nc --show-progress https://releases.linaro.org/components/toolchain/binaries/latest-7/aarch64-linux-gnu/gcc-linaro-7.5.0-2019.12-x86_64_aarch64-linux-gnu.tar.xz
RUN tar xf gcc-linaro-7.5.0-2019.12-x86_64_aarch64-linux-gnu.tar.xz
RUN cp -r gcc-linaro-7.5.0-2019.12-x86_64_aarch64-linux-gnu/* /usr/local/
RUN rm -rf gcc-linaro-7.5.0-2019.12-x86_64_aarch64-linux-gnu.tar.xz gcc-linaro-7.5.0-2019.12-x86_64_aarch64-linux-gnu/

RUN wget -q -nc --show-progress https://releases.linaro.org/components/toolchain/binaries/latest-7/arm-linux-gnueabi/gcc-linaro-7.5.0-2019.12-x86_64_arm-linux-gnueabi.tar.xz
RUN tar xf gcc-linaro-7.5.0-2019.12-x86_64_arm-linux-gnueabi.tar.xz
RUN cp -r gcc-linaro-7.5.0-2019.12-x86_64_arm-linux-gnueabi/* /usr/local/
RUN rm -rf gcc-linaro-7.5.0-2019.12-x86_64_arm-linux-gnueabi/ gcc-linaro-7.5.0-2019.12-x86_64_arm-linux-gnueabi.tar.xz

# Get mkdtimg
RUN wget https://android.googlesource.com/platform/system/libufdt/+archive/refs/heads/master/utils.tar.gz
RUN tar xvf utils.tar.gz
RUN cp src/mkdtboimg.py /usr/bin/mkdtimg
RUN chmod a+x /usr/bin/mkdtimg
RUN rm -rf src tests README.md utils.tar.gz
# Get linaro gcc 7.5.0 elf for UEFI
RUN wget -q -nc --show-progress https://releases.linaro.org/components/toolchain/binaries/latest-7/aarch64-elf/gcc-linaro-7.5.0-2019.12-x86_64_aarch64-elf.tar.xz
RUN tar xf gcc-linaro-7.5.0-2019.12-x86_64_aarch64-elf.tar.xz -C /opt
RUN rm -rf gcc-linaro-7.5.0-2019.12-x86_64_aarch64-elf.tar.xz
# Install powershell.
RUN wget -q https://packages.microsoft.com/config/ubuntu/20.04/packages-microsoft-prod.deb
RUN dpkg -i packages-microsoft-prod.deb
RUN rm packages-microsoft-prod.deb
RUN apt-get update -y && apt-get install -y powershell

# Setup dumb git account
RUN git config --global user.email "you@example.com"
RUN git config --global user.name "Your Name"
RUN git config --global color.ui false
# Set cross compiler in PATH and CROSS_COMPILE string
ENV CROSS_COMPILE=${CROSS_COMPILE:-"aarch64-linux-gnu-"}
ENV CROSS_COMPILE_arm=${CROSS_COMPILE_arm:-"arm-linux-gnueabi-"}
ENV ARCH=arm64

INCLUDE+ dockerfiles/Dockerfile.base
