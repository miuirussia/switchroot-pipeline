#!/bin/bash

build() {
	# Enter src directory which should contain the pkg to build
	cd $1

	# Install needed dependencies
	pacman -Syu --noconfirm
	pacman -S --asdeps --noconfirm $(cat PKGBUILD| grep "makedepends=" | sed 's/.*(\(.*\))/\1/; s/"//g') || true

	# Make sure to set the correct ID for build dir
	chown -R 1000:1000 .

	# Run makepkg as user
	su - user -c "MAKEFLAGS="-j${CPUS}" makepkg -scf --noconfirm"

	# Copy pkgs to out
	cp *.tar.xz "${OUT}"
}

for pkg in $(ls /build/packages/pkgbuilds/); do
	build "/build/packages/pkgbuilds/${pkg}"
done
