#!/bin/bash

bsp=${bsp:-"https://developer.nvidia.com/embedded/dlc/r32-3-1_Release_v1.0/t210ref_release_aarch64/Tegra210_Linux_R32.3.1_aarch64.tbz2"}
jetson_api=${jetson_api:-"https://repo.download.nvidia.com/jetson/t210/pool/main/n/nvidia-l4t-jetson-multimedia-api/nvidia-l4t-jetson-multimedia-api_32.3.1-20191209225816_arm64.deb"}
configs=${configs:-"https://gitlab.com/l4t-community/gnu-linux/switchroot-pipeline/-/archive/master/switchroot-pipeline-master.tar?path=switch-l4t-configs"}
zstd=${zstd:-"false"}

if [[ $zstd == "false" ]]; then zstd=""
else zstd="-I zstd"; fi

mkdir -p ${OUT}/bsp
cd ${OUT}

wget ${bsp}
wget ${jetson_api}

tar xf ${bsp##*/}
mv Linux_for_Tegra/bootloader/*.deb ${OUT}
mv Linux_for_Tegra/kernel/*.deb ${OUT}
mv Linux_for_Tegra/nv_tegra/l4t_deb_packages/*.deb ${OUT}
#mv Linux_for_Tegra/tools/*.deb ${OUT}

wget ${configs} -O switch-l4t-configs.tar
tar xf switch-l4t-configs.tar -C ${OUT}
mv switchroot-pipeline-master-switch-l4t-configs/switch-l4t-configs/ ${OUT}

for deb in ${OUT}/*.deb; do
	7z x ${deb}
	tar ${zstd} -xvf data.tar.* -C ${OUT}/bsp
	rm -rf _gpgbuilder ${deb} data.* control.* debian-binary
done

if [[ "${device}" = "icosa" ]] || [[ "${device}" = "nano" ]]; then
	pushd ${OUT}/bsp
	mkdir -p usr/lib64 usr/lib/alsa/init/ usr/lib64/libv4l/plugins/ usr/lib/
	mv lib/* usr/lib/

	# Xorg to lib64
	cp -r usr/lib/xorg usr/lib64/
	cp -r ${OUT}/switch-l4t-configs/nvidia-l4t-bsp/{firmware,init}/* .

	# BSP 32.6+
	if [[ ${bsp_ver} == "32.7.4" ]]; then
		ln -s ../tegra21x/nv_acr_ucode_prod.bin usr/lib/firmware/gm20b/acr_ucode.bin
	fi

	# V4L2
	ln -s /usr/lib/aarch64-linux-gnu/tegra/libv4l2_nvvidconv.so usr/lib64/libv4l/plugins/libv4l2_nvvidconv.so
	ln -s /usr/lib/aarch64-linux-gnu/tegra/libv4l2_nvvideocodec.so usr/lib64/libv4l/plugins/libv4l2_nvvideocodec.so
	ln -s /usr/lib/aarch64-linux-gnu/tegra/libv4l2.so.0 usr/lib64/libv4l/plugins/libv4l2.so.0
	ln -s /usr/lib/aarch64-linux-gnu/tegra/libv4lconvert.so.0  usr/lib64/libv4l/plugins/libv4lconvert.so.0  

	# Weston
	#ln -sf /usr/lib/aarch64-linux-gnu/tegra/weston/hmi-controller.so usr/lib/aarch64-linux-gnu/weston/hmi-controller.so
	#ln -sf /usr/lib/aarch64-linux-gnu/tegra/weston/libweston-6.so.0 usr/lib64/libweston-6.so.0
	#ln -sf /usr/lib/aarch64-linux-gnu/tegra/weston/libweston-desktop-6.so.0 usr/lib64/libweston-desktop-6.so.0
	rm -rf usr/bin/weston* usr/lib/aarch64-linux-gnu/tegra/weston/

	# Alsa
	mv usr/share/alsa/init/postinit usr/lib/alsa/init/

	# Configs
	echo -e "/usr/lib\n/usr/lib64\n/usr/lib/aarch64-linux-gnu/tegra\n/usr/lib/aarch64-linux-gnu/tegra-egl" >> etc/ld.so.conf.d/nvidia-tegra.conf
	echo "switchroot" > etc/hostname
	echo -e "bluedroid_pm\nnvhost_vi\nnvgpu\nbrcmfmac\nbtbcm" >> etc/modules

	# Nvpmodel
	sed -i 's/OnlyShowIn.*//g' etc/xdg/autostart/nvpmodel_indicator.desktop

	# L4S
	if [[ "${device}" = "icosa" ]]; then
		cp -r ${OUT}/switch-l4t-configs/nvidia-l4t-bsp/nvpmodel/* .
		rm -rf boot/ usr/lib/modules/
	fi

	rm -rf lib/ \
		usr/lib/systemd/system/bluetooth.service.d/ \
		etc/systemd/system/multi-user.target.wants/ \
		etc/systemd/nvfb.sh \
		etc/systemd/system/nvfb-early.service \
		etc/systemd/system/multi-user.target.wants/nvfb-early.service \
		etc/systemd/system/apt-daily.timer.d \
		etc/systemd/system/apt-daily-upgrade.timer.d \
		etc/systemd/resolved.conf.d \
		etc/systemd/timesyncd.conf.d \
		opt/nvidia/l4t-usb-device-mode/ \
		etc/systemd/nvzramconfig.sh \
		etc/systemd/system/nvmemwarning.service \
		etc/systemd/nvfb.sh \
		etc/systemd/nvgetty.sh \
		etc/systemd/system/nv-l4t-usb-device-mode-runtime.service \
		etc/systemd/system/nv-l4t-usb-device-mode.service \
		etc/systemd/system/multi-user.target.wants/nv-l4t-usb-device-mode.service \
		etc/systemd/system/multi-user.target.wants/nvgetty.service \
		etc/systemd/system/nvgetty.service \
		etc/systemd/system/nvmemwarning.service \
		etc/systemd/system/multi-user.target.wants/nvmemwarning.service \
		etc/systemd/system/nvfb.service \
		etc/systemd/system/multi-user.target.wants/nvfb.service \
		etc/systemd/system/nvzramconfig.service \
		etc/systemd/system/multi-user.target.wants/nvzramconfig.service \
		opt/nvidia/l4t-bootloader-config/ \
		opt/nvidia/l4t-usb-device-mode/ \
		etc/systemd/system/multi-user.target.wants/nv-l4t-bootloader-config.service \
		etc/udev/rules.d/99-nv-l4t-usb-device-mode.rules \
		etc/udev/rules.d/99-nv-ufs-mount.rules \
		etc/udev/rules.d/99-nv-wifibt.rules \
		etc/systemd/nvwifibt-pre.sh \
		etc/systemd/nvwifibt.sh \
		etc/systemd/system/nvwifibt.service \
		usr/lib/firmware/brcm/BCM.hcd \
		usr/share/alsa/init \
		etc/xdg/autostart/nvbackground.* \
		etc/xdg/autostart/nvchrome.* \
		etc/xdg/autostart/nvl4t-readme.sh \
		etc/xdg/autostart/nvlxde* \
		etc/wpa_supplicant.conf
	popd

	rm -rf ${bsp##*/} Linux_for_Tegra/ switch-l4t-configs.tar switchroot-pipeline-master-switch-l4t-configs/ switch-l4t-configs
	tar czf "${OUT}/l4s_bsp_${device}_${bsp_ver}.tar.gz" -C ${OUT}/bsp .
else 7z a l4s-bsp-${device}-${bsp_ver}.7z * -x'!*.7z'; fi
