#!/bin/bash

# Output dir
mkdir -p "${OUT}/ffmpeg_bionic/"

# Setup ccache
/usr/sbin/update-ccache-symlinks
export PATH="/usr/lib/ccache:$PATH"

# Get sources
apt update
apt-get source ffmpeg

# Update and get version
export VERSION_FULL=$(apt show ffmpeg | grep 'Version:' | awk '{print $2}')
export VERSION_SHORT=$(echo $VERSION_FULL | sed 's/.*://; s/\-.*//')
export VERSION_NO_PREFIX=$(echo ${VERSION_FULL} | sed 's/.*://')
if [[ -n ${NEW_NUM} ]]; then
	export NEW_VERSION=$(echo ${VERSION_NO_PREFIX} | sed '0,/-[[:digit:]]/{s/-[[:digit:]]/-'${NEW_NUM}'/}')
else
	export NEW_VERSION=${VERSION_NO_PREFIX}
fi

cd ffmpeg-${VERSION_SHORT}

# Apply nvv4l2 patches
sed -i 's/CONFIG := /CONFIG := --extra-cflags="-march=armv8-a+simd+crypto+crc -mtune=cortex-a57" \\\n\t/g' debian/rules
sed -i 's/CONFIG := /CONFIG := --enable-nvv4l2 \\\n\t/g' debian/rules
patch -p1 -i /build/patches/userspace/ffmpeg/nvv4l2-3.4.11-v6-system.patch

# Ignore querrying package info
echo -e "\noverride_dh_shlibdeps:\n\tdh_shlibdeps --dpkg-shlibdeps-params=--ignore-missing-info" >> debian/rules

# Commit dpkg changes
EDITOR=/bin/true dpkg-source -q --commit . nvv4l2.patch

# Bump version
sed -i "s/${VERSION_FULL}/9:${NEW_VERSION}l4t/g" debian/changelog
find . -type f -exec sed -i 's/'${VERSION_FULL}'/9:'${NEW_VERSION}'l4t/g' {} \;

# Build debs
DEB_BUILD_OPTIONS=nocheck dpkg-buildpackage -j"${CPUS}"
cd ..

# Copy build to output
cp -r *.deb "${OUT}/ffmpeg_bionic/"
