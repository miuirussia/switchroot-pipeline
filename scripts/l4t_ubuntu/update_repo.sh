#!/bin/bash

cd /ext
apt-ftparchive --arch arm64 packages pool/main > dists/bionic/main/binary-arm64/Packages
gzip -f -k dists/bionic/main/binary-arm64/Packages
apt-ftparchive contents pool/main > dists/bionic/main/Contents-arm64
gzip -f -k dists/bionic/main/Contents-arm64
apt-ftparchive release dists/bionic/main/binary-arm64 > dists/bionic/main/binary-arm64/Release
apt-ftparchive release -c release.conf dists/bionic > dists/bionic/Release
find . -type f -exec chmod a+r {} \;
find . -type d -exec chmod a+rx {} \;
