#!/bin/bash
set -e

# Build variables
export KERNEL_DIR="/out/kernel"
export PATCHDIR="/build/patches/kernel"

prepare() {
	mkdir -p "${KERNEL_DIR}"

	echo -e "Cloning L4T merged kernel from l4t-community if missing\n"
    rm -rf "${KERNEL_DIR}/kernel-4.9/" || true
	git clone -b ${KERNEL_BRANCH} https://gitlab.com/l4t-community/kernel/l4t-kernel-4.9 "${KERNEL_DIR}/kernel-4.9/"
	cd "${KERNEL_DIR}/kernel-4.9/"
}

createMergedTree() {
    rm -rf "${KERNEL_DIR}/nvidia" "${KERNEL_DIR}/nvgpu" "${KERNEL_DIR}/hardware" || true
    git clone --depth 1 -b ${NVIDIA_BRANCH} https://gitlab.com/l4t-community/kernel/switch-l4t-kernel-nvidia "${KERNEL_DIR}/nvidia"
    git clone --depth 1 -b ${NVGPU_BRANCH} https://gitlab.com/switchroot/kernel/l4t-kernel-nvgpu "${KERNEL_DIR}/nvgpu"
    git clone --depth 1 -b ${NX_DTB_BRANCH} https://github.com/CTCaer/switch-l4t-platform-t210-nx "${KERNEL_DIR}/hardware/nvidia/platform/t210/nx"
    git clone --depth 1 -b ${PLAT_SOC_VER} https://gitlab.com/switchroot/kernel/l4t-soc-t210 "${KERNEL_DIR}/hardware/nvidia/soc/t210"
    git clone --depth 1 -b ${PLAT_SOC_VER} https://gitlab.com/switchroot/kernel/l4t-soc-tegra "${KERNEL_DIR}/hardware/nvidia/soc/tegra/"
    git clone --depth 1 -b ${PLAT_SOC_VER} https://gitlab.com/switchroot/kernel/l4t-platform-tegra-common "${KERNEL_DIR}/hardware/nvidia/platform/tegra/common/"
    git clone --depth 1 -b ${PLAT_SOC_VER} https://gitlab.com/switchroot/kernel/l4t-platform-t210-common "${KERNEL_DIR}/hardware/nvidia/platform/t210/common/"
}

createUpdateModules() {
	echo -e "Create compressed modules and update archive with correct permissions and ownership\n"
	find "$1" -type d -exec chmod 755 {} \;
	find "$1" -type f -exec chmod 644 {} \;
	find "$1" -name "*.sh" -type f -exec chmod 755 {} \;
	fakeroot chown -R root:root "$1"
	tar -C "$1" -czvpf "$2" .
}

build() {
	echo -e "Preparing Source and Creating Defconfig\n"

	make tegra_linux_defconfig
	make -j"${CPUS}" prepare tegra-dtstree="../hardware/nvidia"
	make -j"${CPUS}" zImage tegra-dtstree="../hardware/nvidia"
	make -j"${CPUS}" dtbs tegra-dtstree="../hardware/nvidia"
	make -j"${CPUS}" modules tegra-dtstree="../hardware/nvidia"

	mkimage -A arm64 -O linux -T kernel -C gzip -a  0x80200000 -e 0x80200000 -n "AZKRN-${KERNEL_BRANCH}" -d ${KERNEL_DIR}/kernel-4.9/arch/arm64/boot/zImage "${OUT}/uImage"

	mkdtimg create "${OUT}/nx-plat.dtimg" --page_size=1000 \
        ${KERNEL_DIR}/kernel-4.9/arch/arm64/boot/dts/tegra210-odin.dtb    --id=0x4F44494E \
		${KERNEL_DIR}/kernel-4.9/arch/arm64/boot/dts/tegra210b01-odin.dtb --id=0x4F44494E --rev=0xb01 \
		${KERNEL_DIR}/kernel-4.9/arch/arm64/boot/dts/tegra210b01-vali.dtb --id=0x56414C49 \
		${KERNEL_DIR}/kernel-4.9/arch/arm64/boot/dts/tegra210b01-fric.dtb --id=0x46524947

	make modules_install INSTALL_MOD_PATH="${KERNEL_DIR}/modules/" tegra-dtstree="../hardware/nvidia"
	make headers_install INSTALL_HDR_PATH="${KERNEL_DIR}/update/usr/" tegra-dtstree="../hardware/nvidia"

	cp "${KERNEL_DIR}/kernel-4.9/vmlinux" "${OUT}"

	make -j"${CPUS}" clean tegra-dtstree="../hardware/nvidia"
}

postConfig() {
	echo -e "Refresh permissions for kernel headers and Create compressed modules and headers\n"
	find "${KERNEL_DIR}/update/usr/include" -exec chmod 777 {} \;
	find "${KERNEL_DIR}/update/usr/include" \
	\( -name .install -o -name .check -o \
        -name ..install.cmd -o -name ..check.cmd \) | xargs rm -f

	find "${KERNEL_DIR}/modules/lib" -name "*.ko" -type f -exec aarch64-linux-gnu-strip --strip-debug {} \;

	ln -s /usr/src/kernel-4.9 "${KERNEL_DIR}/modules/lib/modules/4.9.140-l4t+/build"

	createUpdateModules "${KERNEL_DIR}/modules/lib/" "${OUT}/modules.tar.gz"
	createUpdateModules "${KERNEL_DIR}/update/" "${OUT}/update.tar.gz"

	rm -rf "${KERNEL_DIR}/modules" "${KERNEL_DIR}/update"
	echo "Done"
}

createKernelSources() {
    cd "${KERNEL_DIR}"
    sed -i 's|source "drivers/firmware/tegra/Kconfig"|#source "drivers/firmware/tegra/Kconfig"|g' kernel-4.9/arch/arm64/Kconfig
    sed -i 's|source "drivers/net/ethernet/nvidia/eqos/Kconfig"|#source "drivers/net/ethernet/nvidia/eqos/Kconfig"|g' kernel-4.9/drivers/net/ethernet/nvidia/Kconfig
    7z a '-xr!.git' '-xr!.*' '-xr!*.o' '-xr!*.dtb' '-xr!vmlinux' '-xr!Image' '-xr!zImage' "${OUT}/kernel-4.9-src.7z" kernel-4.9
}

#
# PREPARE
#
prepare

#
# STUB FOR ALT. BRANCHES SETUP
#
createMergedTree

#
# BUILD
#
build

#
# PACKAGE
#
postConfig

#
# KERNEL SRC 
#
createKernelSources
