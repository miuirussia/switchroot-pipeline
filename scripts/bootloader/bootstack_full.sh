#!/bin/bash
set -e

if [[ ! -d "${OUT}" ]]; then
        echo "${OUT} is not a valid directory.."
        exit 1
fi

if [[ -z ${DISTRO} ]]; then
        echo "DISTRO=${DISTRO} not set correctly.."
        exit 1
fi

export HKT_ID=SWR-$(echo ${DISTRO:0:3} | tr '[:lower:]' '[:upper:]')

#
# Uenv
#
# Create switchroot and bootloader directories
mkdir -p ${OUT}/switchroot/${DISTRO} ${OUT}/bootloader/ini/

# Edit + copy ini
cp /build/scripts/bootloader/assets/ini ${OUT}/bootloader/ini/L4T-${DISTRO}.ini
sed -i 's/@DISTRO@/'${DISTRO}'/g; s/@HKT_ID@/'${HKT_ID}'/g' ${OUT}/bootloader/ini/L4T-${DISTRO}.ini
if [[ -n "${MAINLINE}" ]]; then
	sed -i 's/mainline=0/mainline=1/g' ${OUT}/bootloader/ini/L4T-${DISTRO}.ini
fi

# Copy bl31 and bl33
cp /build/scripts/bootloader/assets/bl31.bin \
	/build/scripts/bootloader/assets/bl33.bin \
   	${OUT}/switchroot/${DISTRO}/

# Edit + copy boot.scr
sed -i 's/setenv id.*$/setenv id '${HKT_ID}'/g' /build/scripts/bootloader/assets/boot.txt
mkimage -A arm64 -T script -d /build/scripts/bootloader/assets/boot.txt ${OUT}/switchroot/${DISTRO}/boot.scr

# Icons
if [[ -d "/build/scripts/bootloader/assets/Hekate-Icons/${DISTRO}" ]]; then
        cp /build/scripts/bootloader/assets/Hekate-Icons/${DISTRO}/logo_${DISTRO}.bmp ${OUT}/switchroot/${DISTRO}/logo_${DISTRO}.bmp
        cp /build/scripts/bootloader/assets/Hekate-Icons/${DISTRO}/icon_${DISTRO}_hue.bmp ${OUT}/switchroot/${DISTRO}/icon_${DISTRO}_hue.bmp
else
        cp /build/scripts/bootloader/assets/Hekate-Icons/linux/logo_linux.bmp ${OUT}/switchroot/${DISTRO}/logo_${DISTRO}.bmp
        cp /build/scripts/bootloader/assets/Hekate-Icons/linux/icon_linux_hue.bmp ${OUT}/switchroot/${DISTRO}/icon_${DISTRO}_hue.bmp
fi

# Compress
if [[ ${DISTRO} != "ubtouch" ]]; then
        7z a ${OUT}/switchroot-${DISTRO}-boot.7z ${OUT}/bootloader/ ${OUT}/switchroot/
else
        7z a -tzip ${OUT}/switchroot-${DISTRO}-boot.zip ${OUT}/bootloader/ ${OUT}/switchroot/
fi
rm -rf ${OUT}/bootloader/ ${OUT}/switchroot/

echo -e "\nDone building! Output: ${OUT}/switchroot-${DISTRO}-boot.7z"
