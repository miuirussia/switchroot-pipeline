#!/bin/bash
set -e

export PATH="/opt/gcc-linaro-7.5.0-2019.12-x86_64_aarch64-elf/bin/:$PATH"
export CC="aarch64-elf-gcc"

git clone -b edk2-stable202202 https://github.com/tianocore/edk2
cd edk2
git submodule update --init --recursive
git clone https://gitlab.com/l4t-community/bootstack/NintendoSwitchPkg

source edksetup.sh
make -j$(nproc) -C BaseTools
cp ./NintendoSwitchPkg/Tools/run-build.sh .
./run-build.sh
cp Build/NintendoSwitch-AARCH64/DEBUG_GCC5/FV/ELF/UEFI.elf "${OUT}/bl33.bin"
cp /build/scripts/bootloader/assets/uefi.ini "${OUT}"
